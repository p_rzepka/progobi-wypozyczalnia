/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 1 grudnia 2016, 17:18
 */
#include "../../biblioteka/include/Pojazd.h"
#include "../../biblioteka/include/PojazdSilnikowy.h"
#include "../../biblioteka/include/Samochod.h"
#include "../../biblioteka/include/Motor.h"
#include "../../biblioteka/include/Rower.h"
#include "../../biblioteka/include/TypKlienta.h"
#include "../../biblioteka/include/Klient.h"
#include "../../biblioteka/include/Wypozyczenie.h"
#include "../../biblioteka/include/AplicationClass.h"



#include <cstdlib>
#include <iostream>
#include <math.h>
#include <vector>
#include <memory>


using namespace std;



int main() {


    AplicationClass* app=new AplicationClass();
    
    app->loadCustomers();
    app->loadVehicles();
    app->loadWypoB();
    app->loadWypoArch();
   
    //  start aplikacji, wyswietlenie menu
    app->startAplication();
    
    //  zapisanie zmian 
    //app->saver();
    
    

    
    
getchar();
    return 0;
}

