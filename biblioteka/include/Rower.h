/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Rower.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 1 grudnia 2016, 17:34
 */

#ifndef ROWER_H
#define ROWER_H

#include "../include/Pojazd.h"

class Rower :public Pojazd {

private:
    float pojemnosc;

public:
    //Konstruktory i destruktory
    Rower();
    Rower(std::string nazwa, float cenaPojazdu);
    virtual ~Rower();
    
    //settery
    
    //gettery
    std::string getSTRcenaBazowa();
    std::string getSTRcena();
    std::string getSTRpojemnosc();
    std::string getSTRsegment();


};

#endif /* ROWER_H */

