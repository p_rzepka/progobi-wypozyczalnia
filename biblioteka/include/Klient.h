/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Klient.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 3 grudnia 2016, 14:43
 */

#ifndef KLIENT_H
#define KLIENT_H

#include <string>
#include <memory>

#include "TypKlienta.h"

class Klient {
private:
    std::string imie;
    std::string nazwisko;
    float       obrot;
    float       upust;
    TypKlienta *typKlienta; 
    
    /*
     * Wskaznik typKlienta jest wskaznikiem klasy abstarakcyjnej TypKlienta,
     * ktory moze wskazywac na swoje pochodne funkcje okreslajace typ klienta
     * i posiadaja funkcje polimorficzne (f-cje te posiadaja taka sama nazwe,
     * jak funkcja virtualna w klasie abstrakcyjnej, jednak ich dzialania sie roznia.
     * Funkcja ktora sie wykona, jest zalezna od tego na jaka klase pochodną
     * wskazuje wskaznik typKlienta.
     * 
     */
   
public:
    //konstruktory i destruktory
    Klient();
    Klient(std::string imie, std::string nazwisko);
    Klient(std::string imie, std::string nazwisko, char typ);
    Klient(std::string imie, std::string nazwisko, float obrot, float upust, char typ);
    virtual ~Klient();
    
    //gettery
    std::string getName();
    std::string getImie();
    std::string getNazwisko();
    std::string getDaneKlienta();
    std::string getOpisTypKlienta();
    int         getIloscPojazdow();
    float       getObrot();
    float       getUpust();
    char        getTypKlienta();
    
    //settery
    //void setImie(std::string imie);
    //void setNazwisko(std::string nazwisko);
    void   setUpust(float nowyUpust);
    void   setObrot(float nowyObrot);
    void   setWyliczonyNowyUpust();
    
    bool setTypKlienta(int nrTypuKlienta);

};

#endif /* KLIENT_H */

