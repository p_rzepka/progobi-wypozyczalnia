/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Samochod.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 1 grudnia 2016, 17:27
 */

#ifndef SAMOCHOD_H
#define SAMOCHOD_H

#include "PojazdSilnikowy.h"


//  enum segmentu auta segA=0, segB=1, segC=2, segD=3, segE=4
enum seg {segA,segB,segC,segD,segE};

class Samochod : public PojazdSilnikowy{

private:
    seg segment;
        
    
public:
    
    //Konstruktory i destruktory
    Samochod();
    Samochod(std::string nazwa, float pojemnosc, float cenaPojazdu, enum seg auta);
    virtual ~Samochod();

    //settery
    void setCenaZaDzien(); 
    //void setSegment(enum segment auta);

    //gettery
    char getSegment();
    std::string getSTRcenaBazowa();
    std::string getSTRcena();
    std::string getSTRpojemnosc();
    std::string getSTRsegment();
};

#endif /* SAMOCHOD_H */

