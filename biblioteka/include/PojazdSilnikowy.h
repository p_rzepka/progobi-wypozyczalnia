/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PojazdSilnikowy.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 19 grudnia 2016, 12:02
 */

#ifndef POJAZDSILNIKOWY_H
#define POJAZDSILNIKOWY_H

#include"../include/Pojazd.h"

class PojazdSilnikowy : public Pojazd {
private:
        float pojemnosc;

public:
    // Konstruktory i destruktory
    PojazdSilnikowy();
    PojazdSilnikowy(float pojemnosc);
    PojazdSilnikowy(std::string nazwa, float cenaPojazdu,float pojemnosc);
    virtual ~PojazdSilnikowy();
    
    //Gettery
    float getPojemnosc();
    
    
    //Settery
    void setPojemnosc(float nowaPojemnosc);
    void setCenaZaDzien();

};

#endif /* POJAZDSILNIKOWY_H */

