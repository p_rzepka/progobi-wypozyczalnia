/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pojazd.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 1 grudnia 2016, 17:21
 */

#ifndef POJAZD_H
#define POJAZD_H

#include <string>

class Pojazd {

private:
    std::string nazwa;
    float cenaPojazdu;
    float cenaZaDzien;
    //bool dostepny;

public:
    
    //Konstruktory i dekonstruktory
    Pojazd();
    Pojazd(std::string nazwa, float cenaPojazdu);
    virtual ~Pojazd();
    
    //gettery
    
    //float getPojemnosc();
    float getCenaPojazdu();
    float getCenaZaDzien();
    std::string getNazwa();
   
    virtual std::string getSTRcenaBazowa()=0;
    virtual std::string getSTRcena()=0;
    virtual std::string getSTRpojemnosc()=0;
    virtual std::string getSTRsegment()=0;
    //bool getDostepny();
    
    //settery

    void setCenaPojazdu(float nowaCena);
    //void setDostepnosc(bool czyDostepny);
    void setCenaZaDzien(float cenaZaDzien);

    
   

    

};

#endif /* POJAZD_H */

