/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AplicationClass.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 18 grudnia 2016, 14:07
 */

#ifndef APLICATIONCLASS_H
#define APLICATIONCLASS_H

#include"../include/Wypozyczenie.h"
#include"../include/Pojazd.h"
#include"../include/Klient.h"




#include<map>
#include<vector>


class AplicationClass {
    
private:
    std::map<boost::uuids::uuid,std::shared_ptr<Wypozyczenie>> wypozyczenia_b;
    std::map<boost::uuids::uuid,std::shared_ptr<Wypozyczenie>> wypozyczenia_arch;
    std::vector<Klient*> klienci;
    std::map<int,Pojazd*> pojazdy;
    
    
    
public:
    // Konstruktory i destruktory
    AplicationClass();
    virtual ~AplicationClass();
    
    //  gettery
    void getListaWypozyczenBiezacych();     
    void getListaWypozyczenArchiwalnych();
    void getListaKlientow();                
    void getListaPojazdow();                
    int  getIdPojazdu(std::shared_ptr<Pojazd> p);
    int  getIdKlienta(std::shared_ptr<Klient> k);
    int  getIdKlienta(Klient* k);
    
    void getListaDostepnychPojazdow();

    //  settery
    void noweWypozyczenie();                //
    void noweWypozyczenie(int nrPojazdu, int nrKlienta);                //
    void zakonczWypozyczenie(bool symulacja);             //
    void addKlient();                       //
    void addPojazd();                       //
    void setTypKlienta();                   //
    
    
    //  Funkcja sortujaca mape wypozyczen_b po dacie i wyswietlajaca ja
    void sortujWypBiezPoDacie();
    void sortujWypBiezPoNazwisku();
    void startSimulation();
    
    void startAplication();
    
    void loadCustomers();           //done
    void loadVehicles();            //done
    void loadWypoB();               //done
    void loadWypoArch();            //done
    void saver();                   //done
    void clearscreen();

    
};

#endif /* APLICATIONCLASS_H */

