/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Motor.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 1 grudnia 2016, 17:42
 */

#ifndef MOTOR_H
#define MOTOR_H

#include "Pojazd.h"
#include "PojazdSilnikowy.h"

class Motor : public PojazdSilnikowy {

private:
    
public:
    
    //Konstruktory i destruktory
    Motor();
    Motor(std::string nazwa,float pojemnosc, float cenaPojazdu);
    virtual ~Motor();
    
    //gettery
    std::string getSTRcenaBazowa();
    std::string getSTRcena();
    std::string getSTRpojemnosc();
    std::string getSTRsegment();


};

#endif /* MOTOR_H */

