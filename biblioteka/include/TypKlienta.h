/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TypKlienta.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 2 grudnia 2016, 16:52
 */


/*
 * Klasa abstraktycjna i jej polimorficzne podklasy Typu klienta
 * (wiecej niz jedna klasa jest zawarta w tym pliku!)
 */

/*
 * Do poprawnego dzialania polimorfizmu i wskaznikow, ktore beda obslugiwac
 * klasy pochodne, wymagane jest zdefiniowanie konstruktorow parametrowych 
 * oraz stworzenie danych obiektow za ich pomoca
 */

#ifndef TYPKLIENTA_H
#define TYPKLIENTA_H

#include <string>

//klasa abstrakcyjna
class TypKlienta {
public:

    //GETTERY
    virtual int         getLiczbaPojazdow()=0;
    virtual float       getUpust(float obrot,float upust)=0;
    virtual std::string getOpisTypKlienta()=0;
    virtual char        getTypKlienta()=0;


};


// Typ klienta standard - stala, domyslna znizka i liczba pojazdow = 1
class TypStandard:public TypKlienta {
private:
    int liczbaPojazdow;

public:
    //KONSTRUKTORY I DESTRUKTORY
    TypStandard(int ile);
    ~TypStandard();
    
    //GETTERY
    int getLiczbaPojazdow();
    float getUpust(float obrot,float upust);
    std::string getOpisTypKlienta();
    char getTypKlienta();

    
        
};

// Typ klienta lojalnego - znizka schodkowa i stala liczba pojazdow - 2
class TypLoyal : public TypKlienta {
private:
    int liczbaPojazdow;
public:
    TypLoyal(int ile);
    ~TypLoyal();
    
    //GETTERY
    int getLiczbaPojazdow();
    float getUpust(float obrot,float upust);
    std::string getOpisTypKlienta();
    char getTypKlienta();
    
    //SETTERY
    void setLiczbaPojazdow();

};


// Typ klienta Buisness - znizka rosnaca liniowo
class TypBuisness : public TypKlienta {
private:
    int liczbaPojazdow;
public:
    TypBuisness(int ile);
    ~TypBuisness();
    
    //GETTERY
    int getLiczbaPojazdow();
    float getUpust(float obrot,float upust);
    std::string getOpisTypKlienta();
    char getTypKlienta();
    
};
 

//Typ klienta BuisnessPRO - znizka rosnaca liniowo *1.2
class TypBuisnessPRO : public TypKlienta {
private:
    int liczbaPojazdow;
public:
    TypBuisnessPRO(int ile);
    ~TypBuisnessPRO();
    
    //GETTERY
    int getLiczbaPojazdow();
    float getUpust(float obrot,float upust);
    std::string getOpisTypKlienta();
    char getTypKlienta();
};
 

#endif /* TYPKLIENTA_H */

