/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Wypozyczenie.h
 * Author: Przemysław Rzepka i Radosław Kępa
 *
 * Created on 4 grudnia 2016, 10:06
 */

#ifndef WYPOZYCZENIE_H
#define WYPOZYCZENIE_H

#include "../include/Klient.h"
#include "../include/Pojazd.h"

#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.

class Wypozyczenie {
private:
    boost::uuids::uuid  uuid;              
    time_t  start_date;
    time_t  end_date;
    float   cena;
    std::shared_ptr<Pojazd> pojazd;
    std::shared_ptr<Klient> klient;
    

public:
    
        //konstruktory i destruktory
    Wypozyczenie();
    Wypozyczenie(Klient *klient,Pojazd *pojazd);
    Wypozyczenie(std::shared_ptr<Klient> k,std::shared_ptr<Pojazd> p);
    Wypozyczenie(std::shared_ptr<Klient> k,std::shared_ptr<Pojazd> p,boost::uuids::uuid  uuid,time_t start, time_t end);    //do obslugi odczytu z pliku
    Wypozyczenie(std::shared_ptr<Klient> k,std::shared_ptr<Pojazd> p,boost::uuids::uuid  uuid,time_t start, time_t end, float cena);    //do obslugi odczytu z pliku
    virtual ~Wypozyczenie();
    
    Wypozyczenie operator=(Wypozyczenie const&);
    
        //gettery
    time_t              getStartDate();
    time_t              getEndDate();
    boost::uuids::uuid  getUUID();
    float               getCena();
    std::shared_ptr<Klient>  getKlient();
    std::shared_ptr<Pojazd>  getPojazd();
    float               getDiffTime();
    std::string         toString();

    
        //settery
    void             setUUID();
    void             setStart();
    void             setEnd();
    void             losujWypozyczenie();


};

#endif /* WYPOZYCZENIE_H */

