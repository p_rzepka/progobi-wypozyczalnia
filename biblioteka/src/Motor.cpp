/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Motor.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 1 grudnia 2016, 17:42
 */



#include "../include/Motor.h"
#include "../include/Pojazd.h"

Motor::Motor() {
}

/*
 * Konstruktor parametrowy ustawia cene za dzien wypozyczenia motoru
 */
Motor::Motor(std::string nazwa,float pojemnosc, float cenaPojazdu)
: PojazdSilnikowy(nazwa, cenaPojazdu, pojemnosc){
    
    PojazdSilnikowy::setCenaZaDzien();

}

Motor::~Motor() {
}

/////////// GETTERY /////////////////

std::string Motor::getSTRcenaBazowa(){
      
    //  konwersja ceny pojazdu (float) na string
    std::string cenaBazowaMotoru = std::to_string(Motor::getCenaPojazdu());
    //  usuniecie czterech zer ze stringa
    cenaBazowaMotoru.erase(cenaBazowaMotoru.length()-4,4);
 
        return cenaBazowaMotoru;  
}

std::string Motor::getSTRcena(){
    
    //  konwersja ceny pojazdu (float) na string
    std::string cenaMotoru = std::to_string(Motor::getCenaZaDzien());
    //  usuniecie czterech zer ze stringa
    cenaMotoru.erase(cenaMotoru.length()-4,4);
 
        return cenaMotoru;
}

std::string Motor::getSTRpojemnosc(){
        
    //  konwersja pojemnosci pojazdu (float) na string
    std::string pojemnoscMotoru = std::to_string(Motor::getPojemnosc());
    //  usuniecie szesciu zer ze stringa oraz kopki
    pojemnoscMotoru.erase(pojemnoscMotoru.length()-7,7);
    
    return pojemnoscMotoru;
    
}

std::string Motor::getSTRsegment() {
    
    return "0";
}