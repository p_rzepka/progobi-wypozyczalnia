/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Samochod.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 1 grudnia 2016, 17:27
 */

#include <stdexcept>


#include "../include/Samochod.h"
#include "../include/Pojazd.h"

Samochod::Samochod() {
}

/*
 * Konstruktor parametrowy ustawia segment auta oraz cene za dzien
 * w oparciu o ten segment
 */
Samochod::Samochod(std::string nazwa, float pojemnosc, float cenaPojazdu, enum seg auta) 
: PojazdSilnikowy(nazwa, cenaPojazdu, pojemnosc)
{
    try{
        if(auta!=segA && auta!=segB && auta !=segC && auta !=segD && auta !=segE){
            throw std::range_error("ERROR #162 - Niepoprawny segment auta\n");
        }
    }
        catch(std::range_error &error){
            printf(error.what());
            throw std::range_error("ERROR #999 - Przerywam operacje!\n");
            
        }
            
    
    this->segment=auta;
    Samochod::setCenaZaDzien();
}




Samochod::~Samochod() {
}

///////////////////////////////////GETTERY//////////////////////////////////////
/*
 * Zwraca segment auta jako char.
 * 65=A i do tego dodaje poszczegolne wartosci enum segment
 * Np. segment segB=1, wiec 65+1=66 czyli B w ASCII.
 * 
 */
char Samochod::getSegment() {
    char car=65;
    car+=segment;
    return car;
    }

std::string Samochod::getSTRcenaBazowa(){
    
    //  konwersja ceny pojazdu (float) na string
    std::string cenaBazowaSamochodu = std::to_string(Samochod::getCenaPojazdu());
    
    //  usuniecie z konca stringa 4 cyfr w celu unikniecia wyswietlania zbyt
    //  dlugiej liczby. Np.: 50.000000
    cenaBazowaSamochodu.erase( cenaBazowaSamochodu.size()-4, 4 );


        return cenaBazowaSamochodu;    
}

std::string Samochod::getSTRcena() {
    
    //  konwersja ceny pojazdu (float) na string
    std::string cenaSamochodu = std::to_string(Samochod::getCenaZaDzien());
    
    //  usuniecie z konca stringa 4 cyfr w celu unikniecia wyswietlania zbyt
    //  dlugiej liczby. Np.: 50.000000
    cenaSamochodu.erase( cenaSamochodu.size()-4, 4 );


        return cenaSamochodu;
}
    

std::string Samochod::getSTRpojemnosc() {
    //  usuniecie z konca stringa 7 znakow tj. kropki i szesciu zer.
    std::string pojemnoscSamochodu = std::to_string(Samochod::getPojemnosc());
    pojemnoscSamochodu.erase( pojemnoscSamochodu.size()-7, 7 );
    
        return pojemnoscSamochodu;
}

std::string Samochod::getSTRsegment() {
    
    //  Pobiera segment auta (char) i zwraca jako string
    // w przypadku konwersji to_string zwracana by byla wartosc ASCII
    char seg=Samochod::getSegment();
    
    if(seg=='A'){
        return "A"; 
    }
    if(seg=='B'){
        return "B"; 
    }
    
    if(seg=='C'){
        return "C"; 
    }
        
    if(seg=='D'){
        return "D"; 
    }
    
    if(seg=='E'){
       return "E"; 
    }
    
 
}
                                       

/////////////////////////////////////SETTERY////////////////////////////////////


/*
 * Funkcja ustala mnoznika ceny zaleznie od segmentu auta
 */
 void Samochod::setCenaZaDzien() {
     float cenaSegment;
     
     if(segment==segA) {
        cenaSegment=1.0; 
     }
 
      if(segment==1) {
        cenaSegment=1.1; 
     }
     
      if(segment==2) {
        cenaSegment=1.2; 
     }
     
      if(segment==3) {
        cenaSegment=1.3; 
     }
     
      if(segment==4) {
        cenaSegment=1.5;
     }
    /*
     * Po ustaleniu mnoznika ceny zaleznego od segmentu auta
     * jest on przekazywany do f-cji klasy Pojazd, ktora ustala cene pojazdu
     * za dzien wypozyczenia
     */  
     float cena=Pojazd::getCenaZaDzien();
     cena*=cenaSegment;
     Pojazd::setCenaZaDzien(cena);

     
       
        
    }