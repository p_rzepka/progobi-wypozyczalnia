/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PojazdSilnikowy.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 19 grudnia 2016, 12:02
 */

#include <cstdio>
#include <stdexcept>

#include "../include/PojazdSilnikowy.h"

PojazdSilnikowy::PojazdSilnikowy() {
}

PojazdSilnikowy::PojazdSilnikowy(float pojemnosc) : pojemnosc(pojemnosc) {
    setCenaZaDzien();
    
}

//  W przypadku wykrycia blednych wartosci jest rzucany wyjatek, ktory jest lapany, wyswietlany jest opis, a nastepnie rzucany dalej.
//  Wylapywany jest on w f-cji main i uniemozliwia stworzenie takowego obiektu w przypadku blednych danych.
PojazdSilnikowy::PojazdSilnikowy(std::string nazwa, float cenaPojazdu,float pojemnosc) : Pojazd(nazwa,cenaPojazdu), pojemnosc(pojemnosc) {
    
    try{
            if(pojemnosc<=0)
            {
                throw std::range_error("ERROR #142 - Nieprawidlowa pojemnosc!\n");
            }
            
            if(cenaPojazdu<0)
            {
                throw std::range_error("ERROR #152 - Nieprawidlowa cena!\n");
            }
        }
        
        catch(std::range_error &error){
            printf(error.what());
            throw std::range_error("ERROR #999 - Przerywam operacje!\n");
            getchar();
        }
    
    setCenaZaDzien();
}

PojazdSilnikowy::~PojazdSilnikowy() {
}

/////////////GETTERY////////////////////////////////
   /*
    * Pobranie pojemnosci
    */
    float PojazdSilnikowy::getPojemnosc(){
        return pojemnosc;
    }
    
//////////////////SETTERY///////////////////////////

   /*
    * Ustawienie pojemnosci
    */
    void PojazdSilnikowy::setPojemnosc(float nowaPojemnosc) {
        
        try{
            if(nowaPojemnosc<=0)
            {
                throw std::logic_error("ERROR #142 - bledna pojemnosc! (<=0)\n");
            }
        }
        
        catch(std::logic_error &error){
            printf(error.what());
            getchar();
            return;
        }
        pojemnosc=nowaPojemnosc;
    }
    
    /*
    * Ustawienie ceny za dzien wypozyczenia pojazdu zaleznej od pojemnosci.
    * Uruchamiany wraz z konstruktorem pojazu!
    */
    void PojazdSilnikowy::setCenaZaDzien() {
        float znizkaPojemnosc;
    
        if(pojemnosc<=1000) {
            znizkaPojemnosc=1.0;
        }
    
        if(pojemnosc>1000 && pojemnosc < 2000) {
            znizkaPojemnosc=(pojemnosc/1000)-1.0;
            znizkaPojemnosc=1+(znizkaPojemnosc*0.5);
        }
    
        if(pojemnosc>=2000) {
            znizkaPojemnosc=1.5;
        }
        float cenaZaDzien;
        cenaZaDzien=Pojazd::getCenaPojazdu();
        Pojazd::setCenaZaDzien(cenaZaDzien*znizkaPojemnosc);
    
    }
    