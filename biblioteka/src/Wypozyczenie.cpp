/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Wypozyczenie.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 4 grudnia 2016, 10:06
 */

#include "../include/Wypozyczenie.h"
#include "../include/Klient.h"
#include "../include/Pojazd.h"

#include <ctime>
#include <cstdlib>



Wypozyczenie::Wypozyczenie() {
}
/*
 * Obj::Obj( string ) {
 *   if( string == "something" ) {
 *      // should I put this here or only return?
 *      throw ObjectCouldNotBeCreatedException();
 *   }
 *  }
 */
Wypozyczenie::Wypozyczenie(Klient *klient,Pojazd *pojazd) {
//    this->klient=klient;
//    this->pojazd=pojazd;
//    setUUID();
//    setStart();
}

Wypozyczenie::Wypozyczenie(std::shared_ptr<Klient> k,std::shared_ptr<Pojazd> p) {
    
    
    this->klient=k;
    this->pojazd=p;
    
    float cena=pojazd->getCenaZaDzien();
    float upust=klient->getUpust();
    
    this->end_date=NULL;
    this->cena=cena*upust;
    
    setUUID();
    setStart();
            
 
}

Wypozyczenie::Wypozyczenie(std::shared_ptr<Klient> k,std::shared_ptr<Pojazd> p,boost::uuids::uuid  uuid,time_t start, time_t end) {
    float cena=p->getCenaZaDzien();
    float upust=k->getUpust();
    
    this->uuid=uuid;
    this->start_date=start;
    this->end_date=end;
    this->cena=cena*upust;
    this->klient=k;
    this->pojazd=p;

}

Wypozyczenie::Wypozyczenie(std::shared_ptr<Klient> k,std::shared_ptr<Pojazd> p,boost::uuids::uuid  uuid,time_t start, time_t end, float cena) {
 
    this->uuid=uuid;
    this->start_date=start;
    this->end_date=end;
    this->cena=cena;
    this->klient=k;
    this->pojazd=p;

}

Wypozyczenie Wypozyczenie::operator =(Wypozyczenie const& w){
    
    this->uuid=w.uuid;
    this->start_date=w.start_date;
    this->end_date=w.end_date;
    this->cena=w.cena;
    this->klient=w.klient;
    this->pojazd=w.pojazd; 
}




Wypozyczenie::~Wypozyczenie() {
}

///////////////////////////////////////GETTERY//////////////////////////////////
time_t Wypozyczenie::getStartDate() {
    return start_date;
}

time_t Wypozyczenie::getEndDate() {
    return end_date;
}

boost::uuids::uuid Wypozyczenie::getUUID(){
    return uuid;
}

float Wypozyczenie::getCena(){
    return cena;
}
std::shared_ptr<Klient> Wypozyczenie::getKlient(){
    return this->klient;
}

std::shared_ptr<Pojazd> Wypozyczenie::getPojazd(){
    return this->pojazd;
}

float Wypozyczenie::getDiffTime(){
    float roznica=(difftime(end_date,start_date))/86400;
    return roznica;
}

std::string Wypozyczenie::toString() {
    
    std::string retValue;
    
    std::string carPrice=std::to_string(pojazd->getCenaZaDzien());
    carPrice.erase(carPrice.size()-4,4);

    std::string discount=std::to_string(klient->getUpust());
    discount.erase(discount.size()-4,4);
    
    std::string price=std::to_string(cena);
    price.erase(price.size()-4,4);
    
    retValue.append("Cena za dzien pojazdu: ")
            .append(carPrice)
            .append(", a upust klienta: ")
            .append(discount)
            .append("\nWyliczona cena za dzien wypozyczenia pojazdu wynosi: ")
            .append(price);
    
    return retValue;

}



////////////////////////////////////SETTERY/////////////////////////////////////
/*
 * Ustawienie unikalnego identyfikatora
 */
void Wypozyczenie::setUUID() {
    uuid = boost::uuids::random_generator()();
}

/*
 * Ustawienie daty rozpoczecia wypozyczenia
 */
void Wypozyczenie::setStart() {
    start_date=time(0);
}

/*
 * Ustawienie daty zakonczenia wypozyczenia
 */
void Wypozyczenie::setEnd() {
    end_date=time(0);
}
 
/*
 * Ustawienie losowej daty wypozyczenia, do maks 10 dni wstecz.
 */
void Wypozyczenie::losujWypozyczenie() {
    unsigned int dniWstecz,mnoznik;
    
    srand( time( NULL ) );
    /*
     * Ustawienie liczby sekund odpowiadajacej jednemu dniu
     */
    //dniWstecz = 86400;
     
    /*
     * Losujemy mnoznik z przedzialu 1 do 10, po czym mnozymy przez niego
     * liczbe sekund odpowiadajacy jednemu dniu
     */
    //mnoznik = (std::rand() % 10 ) + 1 ;
    //std::cout<<"wylosowano: "<<mnoznik<<" mnoznik \n";
    
    
    mnoznik = (std::rand() % 6912 ) + 864 ;
    //std::cout<<"Mnoznik = "<<mnoznik<<std::endl;
     int mnoznik2;
    mnoznik2 = (std::rand() % 100 ) + 0 ;
    //std::cout<<"Mnoznik 2= "<<mnoznik2<<std::endl;
    mnoznik*=mnoznik2;
    //std::cout<<"Mnoznik = "<<mnoznik<<std::endl;
    
    
    //dniWstecz*=mnoznik;
     
    
    //start_date-=dniWstecz;  
    start_date-=mnoznik;
}