/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Klient.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 3 grudnia 2016, 14:43
 */

#include "../include/Klient.h"

#include <iostream>
#include <memory>
#include <stdexcept>
#include <cstdio>



Klient::Klient() {
}

/*
 * Konstruktor parametrowy.
 * Przekazujemy tylko imie i nazwisko, a konstruktor 
 * domyslnie ustawia obrot=0, upust=1.0 (czyli 100% ceny)
 * oraz typ klienta ze stalym upustem i maksymalna liczba wypozyczonych aut na 1
 */
Klient::Klient(std::string imie, std::string nazwisko)
:imie(imie),nazwisko(nazwisko) {
    obrot=0.0;
    upust=1.0;
    typKlienta=new TypStandard(1);

}

Klient::Klient(std::string imie, std::string nazwisko, char typ)
:imie(imie),nazwisko(nazwisko) {
    obrot=0.0;
    upust=1.0;
    
    try{
        if(typ!='A' && typ!='B' && typ!='C' && typ!='D' && typ!='E'){
            throw std::logic_error("ERROR #13 - nierozpoznany typ klienta\nUstawiono domyslny typ A\n");
        }
    }
    
    catch(std::logic_error &error){     
            
        std::cout<<error.what();
            typ='A';
            getchar();  
    }
        
        
    if(typ=='A'){
    typKlienta=new TypStandard(1);
    }
    
    if(typ=='B'){
    typKlienta=new TypLoyal(2);
    }
    
    if(typ=='C'){
    typKlienta=new TypBuisness(5);
    }
    
    if(typ=='D'){
    typKlienta=new TypBuisnessPRO(10);
    }
    else {
    typKlienta=new TypStandard(1);
    }
}

Klient::Klient(std::string imie, std::string nazwisko, float obrot, float upust, char typ)
:imie(imie),nazwisko(nazwisko),obrot(obrot),upust(upust) {
    
    try {
        if (obrot < 0){
            throw std::invalid_argument("ERROR #11 - obrot < 0!\nUstawiono domyslnie 0\n");
        }
        
        if (upust < 0){
            throw std::out_of_range("ERROR #12 - upust < 0!\nUstawiono domyslnie 1\n");
        }
        
        if(typ!='A' && typ!='B' && typ!='C' && typ!='D' && typ!='E'){
            throw std::logic_error("ERROR #13 - nierozpoznany typ klienta\nUstawiono domyslny typ A\n");
        }
    }
    
    catch(std::invalid_argument &error){
        std::cout<<error.what();
        this->obrot=0;
        getchar();
    }
    
    catch(std::out_of_range &error){
        std::cout<<error.what();
        this->upust=1;
        getchar();
    }

    catch(std::logic_error &error){     
            
        std::cout<<error.what();
            typ='A';
            getchar();  
    }
    
    if(typ=='A'){
    typKlienta=new TypStandard(1);
    }
    
    if(typ=='B'){
    typKlienta=new TypLoyal(2);
    }
    
    if(typ=='C'){
    typKlienta=new TypBuisness(5);
    }
    
    if(typ=='D'){
    typKlienta=new TypBuisnessPRO(10);
    }
    
}
    

/*
 * Destruktor usuwa (delete) typ klienta, aby zapobiec wyciekowi pamieci
 */
Klient::~Klient() {
    delete typKlienta;
}


//^^^ GETTERY


/*
 * Pobranie imienia 
 */
std::string Klient::getImie() {
    return imie;
}

/*
 * Pobranie  nazwiska
 */
std::string Klient::getNazwisko() {
    return nazwisko;
}
    
/*
 * Pobranie imienia i nazwiska w jednym stringu
 */
std::string Klient::getName() {
    std::string imie,nazwisko,name;
    imie=getImie();
    nazwisko=getNazwisko();
    name=imie+" "+nazwisko;
    return name;
}

/*
 * Pobranie obrotu
 */
float Klient::getObrot() {
    return obrot;
}

/*
 * Pobranie upustu
 */
float Klient::getUpust() {
    return upust;
}

/*
 * Pobranie imienia, nazwiska
 * typ klienta w jednym stringu
 */
std::string Klient::getDaneKlienta() {
    std::string imieNazwisko,typ;
    imieNazwisko=getName();
    typ=typKlienta->getTypKlienta();
    
    /*
     * uzyc zewnetrznej biblioteki boost/lexical_cast w celu 
     * dodania floatow 'obrot' i 'upust' do return stringa
     *
     */
    
    return imieNazwisko+" "+typ+"\n";
}

//  Pobranie i zwrot stringa z opisem typu klienta
std::string Klient::getOpisTypKlienta() {
    std::string typ;
    typ=typKlienta->getTypKlienta();

        return typ;
}

//  Pobranie i zwrot typu klienta w postaci char
char Klient::getTypKlienta() {
    char typ;
    typ=typKlienta->getTypKlienta();
    return typ;       
}

/*
 * Pobranie maksymalnej ilosci pojazdow
 * jaka klient moze rownoczesnie wypozyczyc 
 */
int Klient::getIloscPojazdow() {
    int ile;
    ile=typKlienta->getLiczbaPojazdow();
        return ile;
}

//%%% SETTERY

/*
 * Ustawienie upustu
 */
 void Klient::setUpust(float nowyUpust) {
    
     try{
         if(nowyUpust<=0)
         {
             throw std::logic_error("ERROR #122\nProba ustawienia niedopuszczalnego upustu dla klienta!\n");
         }
     }
     
     catch(std::logic_error &error){
        std::cout<<error.what();
        getchar();
            return;
        }
     
     upust=nowyUpust;
 }
    
/*
 * Ustawienie obrotow
 */
 void Klient::setObrot(float nowyObrot) {
     try{
         if(nowyObrot<0)
         {
             throw std::logic_error("ERROR #112\nProba ustawienia ujemnego obrotu dla klienta!\n");
         }
     }
     
     catch(std::logic_error &error){
         std::cout<<error.what();
         getchar();
         
            return;
    }
     
     obrot=nowyObrot;
 }
 
 /*
  * Zmienia poczatkowy typ klienta.
  * Przed wybraniem nowego typu usuwa (delete) stary typ klienta, aby
  * zapobiec wycieku pamieci.
  * Po wybraniu nowego typu, nalicza nowy upust 
  * w oparciu o sposob naliczania upustu charakterystyczny
  * dla danego typu klienta.
  */
 bool Klient::setTypKlienta(int nrTypuKlienta) {
         
         switch(nrTypuKlienta){
            case 1 : 
                delete typKlienta;
                this->typKlienta=new TypStandard(1);
                setWyliczonyNowyUpust();
                return true;        
           
            case 2 : 
                delete typKlienta;
                this->typKlienta=new TypLoyal(2);
                setWyliczonyNowyUpust();
                return true;
                 
            case 3 : 
                delete typKlienta;
                this->typKlienta=new TypBuisness(5);
                setWyliczonyNowyUpust();
                return true;
             
            case 4 : 
                delete typKlienta;
                this->typKlienta=new TypBuisnessPRO(10);
                setWyliczonyNowyUpust();
                return true;
            
             default : 
                
                return false;
         }

 }

 /*
  * Ustala nowy upust, wyliczony na podstawie
  * sposobu wyliczania upust u danego typu klienta
  */
 void Klient::setWyliczonyNowyUpust() {
     float wyliczonyNowyUpust=typKlienta->getUpust(obrot,upust);
        
     try{
         if(wyliczonyNowyUpust<=0){
             throw std::logic_error("ERROR #112\nProba ustawienia ujemnego obrotu dla klienta!\n");
         }     
     }
     
     catch(std::logic_error &error){
        std::cout<<error.what();
        getchar();
            return;
         }   
     
               
     upust=wyliczonyNowyUpust;
 }