/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AplicationClass.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 18 grudnia 2016, 14:07
 */

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "../include/AplicationClass.h"
#include "../include/Pojazd.h"
#include "../include/Samochod.h"
#include "../include/Motor.h"
#include "../include/Rower.h"
#include "../include/Klient.h"

#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <math.h>

AplicationClass::AplicationClass() {
}


AplicationClass::~AplicationClass() {
}


void AplicationClass::startAplication() {
    
    int wybor=NULL;
    int podWybor=NULL;
    do{
        
        clearscreen();  //czysci ekran
        
   std::cout<<"||||--------  WYPOZYCZALNIA \"CARHIRE\" -------------| \n"
            <<"|nr|-----------------------------------------------| \n"
            <<"| 1| -               ...Klienci...                 | \n"
            <<"|  |-----------------------------------------------| \n"
            <<"| 2| -               ...Pojazdy...                 | \n"
            <<"|  |-----------------------------------------------| \n"
            <<"| 3| -            ### Wypozyczenia ###             | \n"
            <<"|  |-----------------------------------------------| \n"
            <<"|  |                                               | \n"
            <<"| 0| -               !!!Wyjscie!!!                 | \n"
            <<"|__|_______________________________________________| \n"
            <<"99-start symulacji\n";
   std::cin>>wybor;
   switch(wybor){
        case 1: //  podmenu klienta
            do{
                clearscreen();  //czysci ekran
                
                std::cout<<"WYPOZYCZALNIA 'CARHIRE' \n"
                        <<"1 - Wyswietl Klientow \n"
                        <<"2 - Dodaj Klienta \n"
                        <<"3 - Zmien TypKlienta \n"
                        <<" \n"
                        <<"0 - Wroc... \n";
                std::cin>>podWybor;
                switch(podWybor){
                    case 1:
                        clearscreen();  //czysci ekran
                        AplicationClass::getListaKlientow();
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                        
                    case 2:
                        clearscreen();  //czysci ekran
                        AplicationClass::addKlient();
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                        
                    case 3:
                        clearscreen();  //czysci ekran
                        AplicationClass::setTypKlienta();
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                        
                    case 0:              
                        break;
                        
                    default:
                        std::cout<<"ZLY WYBOR! \n"; 
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                }
  
           }while(podWybor !=0);
           podWybor=NULL;
           break;
           
        case 2: //  podmenu pojazdu
            do{
                clearscreen();  //czysci ekran
                
                std::cout<<"WYPOZYCZALNIA 'CARHIRE' \n"
                        <<"1 - Wyswietl Pojazdy \n"
                        <<"2 - Dodaj Pojazd\n"                       
                        <<" \n"
                        <<"0 - Wroc...\n";
                std::cin>>podWybor;
                switch(podWybor){
                    case 1:
                        clearscreen();  //czysci ekran
                        AplicationClass::getListaPojazdow();
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                        
                    case 2:
                        clearscreen();  //czysci ekran
                        try{
                        AplicationClass::addPojazd();
                        }
                        catch(std::range_error &error){
                            printf(error.what());
                            getchar();
                            getchar();
                        }
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;

                    case 0:              
                        break;
                        
                    default:
                        std::cout<<"ZLY WYBOR! \n";  
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                }
            }while(podWybor !=0);
            podWybor=NULL;
            break;
            
       case 3:
            do{
                clearscreen();  //czysci ekran
                
                std::cout<<"WYPOZYCZALNIA 'CARHIRE' \n"
                        <<"1 - Wyswietl Wypozyczenia Biezace \n"
                        <<"2 - Nowe Wypozyczenie \n"
                        <<"3 - Zakoncz Wypozyczenie \n"
                        <<"4 - Wyswietl wypozyczenia Archwialne\n"                       
                        <<" \n"
                        <<"0 - Wroc... \n";
                std::cin>>podWybor;
                switch(podWybor){
                    case 1:
                        char wyborSort;
                        clearscreen();  //czysci ekran
                        AplicationClass::getListaWypozyczenBiezacych();
                        std::cout<<"D - sortowanie po dacie \n"
                                <<"N - sortowanie po nazwisku \n";
                        std::cin>>wyborSort;
                        
                        if(wyborSort=='D' or wyborSort=='d'){
                            clearscreen();  //czysci ekran
                            AplicationClass::sortujWypBiezPoDacie();  
                        }
                        
                        if(wyborSort=='N' or wyborSort=='n'){
                            clearscreen();  //czysci ekran
                            AplicationClass::sortujWypBiezPoNazwisku();                         
                        }
                        
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
                        getchar();
                        getchar();
                        break;
                        
                    case 2:
                        clearscreen();  //czysci ekran
                        AplicationClass::noweWypozyczenie();
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n"; 
                        getchar();
                        getchar();
                        break;
                        
                    case 3:
                        clearscreen();  //czysci ekran
                        AplicationClass::getListaWypozyczenBiezacych();
                        AplicationClass::zakonczWypozyczenie(false);
                        std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n"; 
                        getchar();
                        getchar();
                        break;
                        
                    case 4:
                        clearscreen();  //czysci ekran
                        AplicationClass::getListaWypozyczenArchiwalnych();
                        getchar();
                        getchar();
                        break;

                    case 0:              
                        break;
                        
                    default:
                        std::cout<<"ZLY WYBOR! \n";  
                        getchar();
                        getchar();
                        break;
                }
            }while(podWybor !=0);
            podWybor=NULL;
            break;
           
       case 99:    
           clearscreen();
           startSimulation();
           std::cout<<"Nacisnij klawisz 'any' by kontynuowac... \n";
           getchar();

           break;
  
       default:
           
           break;
           
               
           
   
   }
   
    }while(wybor !=0);
    
}

/////////////////////////   GETTERY ///////////////////////////
void AplicationClass::getListaKlientow(){

    std::string naglowek="Lista Klientow";
    std::string nr="nr" ;
    std::string imieInazwisko="imie i nazwisko" ;
    std::string obrotSTR="obrot" ;
    std::string upustSTR="upust" ;
    std::string typ="typ" ;
    std::string pojazdy="pojazdy";
    Klient* k1;
    std::shared_ptr<Klient> k2;
    

    printf("|----------------    %-17s-----------------|\n",naglowek.c_str());
    printf("|%2s|%20s|%10s|%5s|%5s|%5s| \n",nr.c_str(),imieInazwisko.c_str(),obrotSTR.c_str(),upustSTR.c_str(),pojazdy.c_str(),typ.c_str());
    printf("|------------------------------------------------------|\n");

    
    int i = 0;
    std::string name,opis,obrotSTRval,upustSTRval;
    float obrot;
    float upust;
    int limit;
    int ilosc;
    
    for(const auto& Klient : klienci){
        
        //  odczyt nr id klienta
        //Klient->
      
        //przypisanie danych imiennych i opisu klienta
        name=Klient->getName();
        opis=Klient->getOpisTypKlienta();
        obrot=Klient->getObrot();
        upust=Klient->getUpust();
        
        // konwersja wartosci float (obrot i upust) na mozliwe do wyswietlenia stringi
        obrotSTRval=std::to_string(obrot);
        upustSTRval=std::to_string(upust);
        
        //  usuniecie 4 ostatnich zer.
        obrotSTRval.erase( obrotSTRval.size()-4, 4 );
        upustSTRval.erase( upustSTRval.size()-4, 4 );
        
        //  odczytanie limitu wypozyczen danego klienta
        k1=Klient;
        limit=k1->getIloscPojazdow();
        
        ilosc=0;
        
        int idKlienta=AplicationClass::getIdKlienta(k1);
        //  petla sprawdza ile klientow o takim samym id, znajduje sie w mapie wypozyczen biezacych;
        for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
            
            k2=it->second->getKlient();                  //  smart_ptr na klienta
            int idK=AplicationClass::getIdKlienta(k2);   //  id klienta
            
            if(idKlienta==idK){ilosc++;}
        }
        
        
        //  wydruk sformatowanyc
        //  %2d - 2 miejsca na liczbe porzadkowa
        //  string.c_str zmienia string na wersje mozliwa do wydrukowania
        //  "...printf has no option for std::string, only a C-style string."
        //  %20s znakow dla stringa
        //  i+1 zeby lista zaczynala sie od 1 a nie 0. uwzglednione w dalszych czesciach kodu.
        printf("|%2d|%20s|%10s|%5s|%3d/%3d|%5s|\n",i+1,name.c_str(),obrotSTRval.c_str(),upustSTRval.c_str(),ilosc,limit,opis.c_str());

        i++;
    }
    printf("|______________________________________________________|\n");


}

void AplicationClass::getListaPojazdow() {
    
    int first;
    std::string nazwa,cena,cenaBazowa,pojemnosc,segment;
    std::string zero="0";
    //  roznica dlugosci stringow
    int rs;
    
    printf("|------------------          Lista Pojazdow        -----------------------| \n");
    printf("|nr|           nazwa|cena bazowa|      cena|      pojemnosc|       segment| \n");
    printf("|-------------------------------------------------------------------------| \n");
    
    for(auto it = pojazdy.begin(); it != pojazdy.end(); ++it){
        
        first=it->first;
        nazwa=it->second->getNazwa();
        cenaBazowa=it->second->getSTRcenaBazowa();
        cena=it->second->getSTRcena();
        pojemnosc=it->second->getSTRpojemnosc();
        
        // porownuje dlugosc dwoch stringow (zmienionych na standard C) i zwraca ich roznice
        // jezeli ich roznica jest rowna zero, czyli w naszym przyypadku "0"=="0"
        // moze doprowadzic do bledow, poniewaz wyrazenie "9"=="0" w tym przypadku rowniez jest prawdziwe
        // zaimplementowane tylko w ramach praktyk jezykowych
        rs=std::strcmp(zero.c_str(),pojemnosc.c_str());
        if(rs==0){pojemnosc="nie dotyczy";}
        
        //  sprawdza czy wartosc stringa jest rowne "0"
        //  jezeli tak to podmienia jego wartos na "nie dotyczy"
        segment=it->second->getSTRsegment();
        if(segment=="0"){segment="nie dotyczy";}
        printf("|%2d|%16s|%11s|%10s|%15s|%14s| \n",first,nazwa.c_str(),cenaBazowa.c_str(),cena.c_str(),pojemnosc.c_str(),segment.c_str());
        }
    
        
    
        
        printf("|_________________________________________________________________________| \n");


}

void AplicationClass::getListaDostepnychPojazdow(){
    
      int first;
    std::string nazwa,cena,cenaBazowa,pojemnosc,segment;
    std::string zero="0";
    //  roznica dlugosci stringow
    int rs;
    
    std::shared_ptr<Pojazd> p;
    
    printf("|------------------          Lista Pojazdow        -----------------------| \n");
    printf("|nr|           nazwa|cena bazowa|      cena|      pojemnosc|       segment| \n");
    printf("|-------------------------------------------------------------------------| \n");
    
    for(auto it = pojazdy.begin(); it != pojazdy.end(); ++it){
        bool dostepny=true;
        first=it->first;
        nazwa=it->second->getNazwa();
        cenaBazowa=it->second->getSTRcenaBazowa();
        cena=it->second->getSTRcena();
        pojemnosc=it->second->getSTRpojemnosc();
        
        
        for(auto it=wypozyczenia_b.begin();it!=wypozyczenia_b.end();++it){
            
            p=it->second->getPojazd();
            
            if(nazwa==p->getNazwa()){dostepny=false;};
        }
        
            if(dostepny==true){
                // porownuje dlugosc dwoch stringow (zmienionyfch na standard C) i zwraca ich roznice
                // jezeli ich roznica jest rowna zero, czyli w naszym przyypadku "0"=="0"
                // moze doprowadzic do bledow, poniewaz wyrazenie "9"=="0" w tym przypadku rowniez jest prawdziwe
                // zaimplementowane tylko w ramach praktyk jezykowych
                rs=std::strcmp(zero.c_str(),pojemnosc.c_str());
                if(rs==0){pojemnosc="nie dotyczy";}

                //  sprawdza czy wartosc stringa jest rowne "0"
                //  jezeli tak to podmienia jego wartos na "nie dotyczy"
                segment=it->second->getSTRsegment();
                if(segment=="0"){segment="nie dotyczy";}

                printf("|%2d|%16s|%11s|%10s|%15s|%14s| \n",first,nazwa.c_str(),cenaBazowa.c_str(),cena.c_str(),pojemnosc.c_str(),segment.c_str());
            }
        }
    
        
    
        
        printf("|_________________________________________________________________________| \n");

    
}

void AplicationClass::getListaWypozyczenBiezacych() {
    
        
    int liczbaWypozyczen,limitPojazdow,idKlienta,idPojazdu;
    std::string UUIDstring;
    std::string nazwisko;
    std::string nazwaPojazdu;
    boost::uuids::uuid UUID;
    time_t start;
    std::shared_ptr<Klient> k;
    std::shared_ptr<Pojazd> p;
    
    printf("|-----------------------------------          Lista Wypozyczen Biezacych      ---------------------------------| \n");
    printf("|                                UUID|    klient|  l. wypozyczen|         pojazd| data rozpoczecia wypozyczenia| \n");
    printf("|--------------------------------------------------------------------------------------------------------------| \n");
    
    for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
        
        //first=it->first;                            //  nr 
        UUID=it->second->getUUID();                 //  UUID
        UUIDstring=boost::uuids::to_string(UUID);   //  UUID (string)
        k=it->second->getKlient();                  //  smart_ptr na klienta
        idKlienta=AplicationClass::getIdKlienta(k); //  id klienta do porownania i wyznaczenia ile posiada aktualnych wypozyczen
        nazwisko=k->getNazwisko();                  //  ustawienie nazwiska jako identyfikator klienta
        
        limitPojazdow=k->getIloscPojazdow();
        
        liczbaWypozyczen=0;
        
        
        //  petla sprawdza ile klientow o takim samym id, znajduje sie w mapie wypozyczen biezacych;
        for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
            
            k=it->second->getKlient();                  //  smart_ptr na klienta
            int idK=AplicationClass::getIdKlienta(k);   //  id klienta
            
            if(idKlienta==idK){liczbaWypozyczen++;}
        }
       
        p=it->second->getPojazd();                      //  smart_ptr na pojazd
        nazwaPojazdu=p->getNazwa();                     //  ustawienie nazwy pojazdu jako jego identyfikator
        
         
        start=it->second->getStartDate();
        printf("|%25s|%10s|%13d/%2d|%15s|%30s| \n",UUIDstring.c_str(),nazwisko.c_str(),liczbaWypozyczen,limitPojazdow,nazwaPojazdu.c_str(),ctime (&start));
 
    }
    
        

    
    
    printf("|______________________________________________________________________________________________________________| \n");
  
    
}

void AplicationClass::getListaWypozyczenArchiwalnych() {
    
    int idKlienta,idPojazdu;
    std::string UUIDstring;
    boost::uuids::uuid UUID;
    time_t start;
    time_t end;
    std::shared_ptr<Klient> k;
    std::shared_ptr<Pojazd> p;
    float liczbaDni;
    float koszt;
    float cena;
    std::string liczbaDniSTR;
    std::string kosztSTR;
    std::string nazwisko;
    std::string pojazd;
    
    printf("|------------------------------------------       Lista Wypozyczen Archiwalnych       ------------------------------------------------| \n");
    printf("|                                UUID|         klient|         pojazd|   start wypozyczenia| koniec wypozyczenia|     l. dni|    koszt| \n");
    printf("|-------------------------------------------------------------------------------------------------------------------------------------| \n");
    
    for(auto it = wypozyczenia_arch.begin(); it != wypozyczenia_arch.end(); ++it){
        
        UUID=it->second->getUUID();                 //  1-UUID
        UUIDstring=boost::uuids::to_string(UUID);   //  UUID (string)
        k=it->second->getKlient();                  //  smart_ptr na klienta
        nazwisko=k->getNazwisko();                  //  pobranie nazwiska jako identyfikator klienta
            
        p=it->second->getPojazd();                      //  smart_ptr na pojazd
        pojazd=p->getNazwa();                           //  pobranie nazwy pojazdu jako jego identyfikator
        
        start=it->second->getStartDate();               //  4-data rozpoczecia
   
        end=it->second->getEndDate();                   //  5-data zakonczenia
        liczbaDni=it->second->getDiffTime();            //  6-ilosc dni
        cena=it->second->getCena();
        koszt=cena*liczbaDni;                           //  7-calokowity koszt wypozyczenia
        float zaok_2 = ceilf(koszt * 100) / 100;        //  zaokraglanie do 2 miejsc po przecinku
        koszt=zaok_2;                                   //  przy uzyciu math.h
        
        
        printf("|%25s|%15s|%15s| ",UUIDstring.c_str(),nazwisko.c_str(),pojazd.c_str());
        
        //  wyswietlenie dni
        auto tm = *std::localtime(&start);
        std::cout << std::put_time(&tm, "%d-%m-%Y %H:%M:%S |");
        tm = *std::localtime(&end);
        std::cout << std::put_time(&tm, "%d-%m-%Y %H:%M:%S");
        
        printf(" |%11.1f|%9.2f| \n",liczbaDni,koszt);
 
    }
    
        

    
    
    printf("|_____________________________________________________________________________________________________________________________________| \n");   
    
    
}

int AplicationClass::getIdKlienta(std::shared_ptr<Klient> k){
    
    //  ustawienie idKlienta na 0
    int id=0;

    
    std::string name1,name2;
    //  przypisanie zmiennej name2, danych imiennych klienta 
    name2=k->getName();
    //  zainicjowanie zmiennej n wartoscia rozmiaru vectora 
    int n=klienci.size();
    
    //  przeszukiwanie vectora klientow, pod katem znalezienia klienta o tym samym imieniu i nazwisku
    // jezeli dojdzie do takiej sytuacju to idKlienta jest rowne i
    for(int i=0; i!=n;i++) {
        
        name1=klienci[i]->getName();
        if(name1==name2){
            //std::cout<<"ZNALEZIONO Id Klienta = "<<i<<" ";
            id=i;}
    }
    //std::cout<<id<<" \n";
    return id;
}

int AplicationClass::getIdKlienta(Klient* k){
    
    //  ustawienie idKlienta na 0
    int id=0;
    
    std::string name1,name2;
    //  przypisanie zmiennej name2, danych imiennych klienta 
    name2=k->getName();
    //  zainicjowanie zmiennej n wartoscia rozmiaru vectora 
    int n=klienci.size();
    
    //  przeszukiwanie vectora klientow, pod katem znalezienia klienta o tym samym imieniu i nazwisku
    // jezeli dojdzie do takiej sytuacju to idKlienta jest rowne i
    for(int i=0; i!=n;i++) {
        
        name1=klienci[i]->getName();
        if(name1==name2){
            //std::cout<<"ZNALEZIONO Id Klienta = "<<i<<" ";
            id=i;}
    }
    //std::cout<<id<<" \n";
    return id;
}

int AplicationClass::getIdPojazdu(std::shared_ptr<Pojazd> p){
    int id=0;
    int i;
    std::string name1,name2;
    name1=p->getNazwa();
    auto first=pojazdy.begin();
    for(auto it = pojazdy.begin(); it != pojazdy.end(); ++it){
        
        if(name1==it->second->getNazwa()){
            i=std::distance(first,it);
            //std::cout<<"ZNALEZIONO Id Pojazdu = "<<i+1<<" ";
            id=i+1;
            }
        
        i++;

    }
    //std::cout<<id<<" \n";
    return id;
    
}
//////////////////////////  SETTERY ///////////////////////////

void AplicationClass::setTypKlienta(){
    
    int naliczanie,nrKlienta,typ;
     
    // wyswietlenie listy klientow
    int i=0;
    std::string name,opis;
        for(const auto& Klient : klienci){
     
        //przypisanie danych imiennych i opisu klienta
        name=Klient->getName();
        opis=Klient->getOpisTypKlienta();
        
        //  wydruk sformatowanyc
        //  %2d - 2 miejsca na liczbe porzadkowa
        //  string.c_str zmienia string na wersje mozliwa do wydrukowania
        //  "...printf has no option for std::string, only a C-style string."
        //  %20s - 20 znakow dla stringa
        printf("|%2d|%20s|%5s|\n",i+1,name.c_str(),opis.c_str());

        i++;
    }
    
    std::cout<<"Podaj numer klienta: \n";
    
    std::cin>>nrKlienta;
    nrKlienta-=1;
    std::cout<<"Podaj numer typu klienta \n"
            <<"1 - (A) Typ klienta standard - brak naliczania upustu. Limit pojazdow = 1 \n"
            <<"2 - (B) Typ klienta lojalnego - znizka naliczana schodkowo. Limit pojazdow = 2 \n"
            <<"3 - (C) Typ klienta Buisness - znizka rosnaca liniowo. Limit pojazdow = 5 \n"
            <<"4 - (D) Typ klienta BuisnessPRO - znizka rosnaca liniowo (szybko). Limit pojazdow = 10 \n";
    std::cin>>typ;
    klienci[nrKlienta]->setTypKlienta(typ);
    
    
    //  wyswietlnie zmian typu klienta
    std::cout<<klienci[nrKlienta]->getName()<<" ustawiony nowy typ klienta na : ";
    std::cout<<klienci[nrKlienta]->getOpisTypKlienta()<<"\n";


  
}

void AplicationClass::zakonczWypozyczenie(bool symulacja){
    


    //  deklaracja zmiennych UUID
    boost::uuids::uuid UUID;
    
    std::string UUID1;
    std::string UUID2;

    if(symulacja==false){
    //  Uzytkownik wprowadza UUID wypozyczenia, ktore chce zakonczyc (lista podgladaowa jest wyswietlana powyzej)
    printf("wprowadz UUID wypozyczenia: \n");
    std::cin>>UUID1;
    }
    std::shared_ptr<Klient> k;

    
    //  w przypadku symulacji!!! ***********************************************
    if(symulacja==true){
        
       
        boost::uuids::uuid randomUUID;
         //  deklaracja mapy z kluczem int
        std::map<int,std::shared_ptr<Wypozyczenie>> wypozyczenia_b_int;
        int i=0;
        //  petla kopiujaca 
        for(auto it=wypozyczenia_b.begin();it!=wypozyczenia_b.end();++it){
            i++;
            wypozyczenia_b_int[i]=it->second;
        }
        int rozmiarMapy=std::distance(wypozyczenia_b_int.begin(),wypozyczenia_b_int.end());
        int nrWyp = (std::rand() % rozmiarMapy)+1;
        auto it=wypozyczenia_b_int[nrWyp];
        randomUUID=it->getUUID();
        UUID1 = boost::lexical_cast<std::string>(randomUUID);
    }
    
    // *************************************************************************
    
    
    //  przeszukiwanie mapy w celu znalezienia wyp. o podanym UUID
    for(auto it=wypozyczenia_b.begin(); it!=wypozyczenia_b.end();++it){
       
        UUID=it->second->getUUID();
        
        //  konversja UUID na std::string, ktory jest pozniej porownywany
        //  z wprowadzonym stringiem
        //  Potrzebne biblioteki:
        //    #include <boost/lexical_cast.hpp>
        //    #include <boost/uuid/uuid_io.hpp>
        UUID2 = boost::lexical_cast<std::string>(UUID);
        
        //  w przypadku znalezienia pasuajcego wyniku
        //  wypozyczenie zostaje skopiowane do mapy wypozyczen archiwalnych
        //  wpis w mapie wyp. biezacych zostaje usuniety
        if(UUID1==UUID2){
   
            //  ustawienie daty zakonczenia, pobranie ilosci dni oraz ceny
            it->second->setEnd();
            float liczbaDni=it->second->getDiffTime();
            float cena=it->second->getCena();
            
            //  pobranie klienta i jego aktualnego obrotu
            k=it->second->getKlient();
            float obrot=k->getObrot();
           
            //  ustawienie nowego obrotu oraz wyliczenie nowego upustu w oparciu
            //  o aktualny obrot. Calkowity koszt wypozyczenia zostaje zaokraglony
            //  do 2 miejsc po przecinku.
            float koszt=(cena*liczbaDni);
            std::cout<<"Calkowity koszt wypozyczenia wynosi: "<<koszt<<std::endl;
            float zaok_2 = ceilf(koszt * 100) / 100;
            koszt=zaok_2;
            std::cout<<"Po zaokragleniu: "<<koszt<<std::endl;
            obrot+=koszt;
            k->setObrot(obrot);
            k->setWyliczonyNowyUpust();
            
            //  skopiowanie wypozyczenia do wypozyczen archiwalnych
            wypozyczenia_arch[UUID]=it->second;
            wypozyczenia_b.erase(UUID);
            printf("Wypozyczenie zakonczone pomyslnie. \n");            

            return;
    
        }
     
    }
           
        printf("Nie znaleziono wypozyczenia o podanym UUID! \n");  

            return;
     


}

void AplicationClass::sortujWypBiezPoDacie(){
    
    //  deklaracje zmiennych
    int liczbaWypozyczen,limitPojazdow,idKlienta,idPojazdu;
    boost::uuids::uuid UUID;
    std::string UUIDstring;
    std::string nazwisko;
    std::string nazwaPojazdu;
    time_t start;
    std::shared_ptr<Klient> k;
    std::shared_ptr<Pojazd> p;
    
    //  deklaracja vectora pary UUID i Wypozyczenie
    std::vector<std::pair<boost::uuids::uuid,std::shared_ptr<Wypozyczenie>>> PoDacie;
    
    //  deklaracja mapy z kluczem daty oraz nazwiska
    std::map<time_t,std::shared_ptr<Wypozyczenie>> wypozyczenia_b_poDacie;

    //  petla kopiujaca (sort po dacie)
    for(auto it=wypozyczenia_b.begin();it!=wypozyczenia_b.end();++it){
    
        wypozyczenia_b_poDacie[it->second->getStartDate()]=it->second;
    }

    //////////////////////  WYSWIETLENIE MAPY POSORTOWANEJ PO DACIE /////////////////////////////////////////////////////
    

    printf("|-----------------------------------          Lista Wypozyczen Biezacych      ---------------------------------| \n");
    printf("|                                UUID|    klient|  l. wypozyczen|         pojazd| data rozpoczecia wypozyczenia| \n");
    printf("|--------------------------------------------------------------------------------------------------------------| \n");
    
    for(auto it = wypozyczenia_b_poDacie.begin(); it != wypozyczenia_b_poDacie.end(); ++it){
        
        //first=it->first;                            //  nr 
        UUID=it->second->getUUID();                 //  UUID
        UUIDstring=boost::uuids::to_string(UUID);   //  UUID (string)
        k=it->second->getKlient();                  //  smart_ptr na klienta
        idKlienta=AplicationClass::getIdKlienta(k); //  id klienta do porownania i wyznaczenia ile posiada aktualnych wypozyczen
        nazwisko=k->getNazwisko();                  //  ustawienie nazwiska jako identyfikator klienta
        
        limitPojazdow=k->getIloscPojazdow();
        
        //  petla sprawdza ile klientow o takim samym id, znajduje sie w mapie wypozyczen biezacych;
        liczbaWypozyczen=0;
        for(auto it = wypozyczenia_b_poDacie.begin(); it != wypozyczenia_b_poDacie.end(); ++it){
            
            k=it->second->getKlient();                  //  smart_ptr na klienta
            int idK=AplicationClass::getIdKlienta(k);   //  id klienta
            
            if(idKlienta==idK){liczbaWypozyczen++;}
        }
       
        p=it->second->getPojazd();                      //  smart_ptr na pojazd
        nazwaPojazdu=p->getNazwa();                     //  ustawienie nazwy pojazdu jako jego identyfikator
        
         
        start=it->second->getStartDate();
        printf("|%25s|%10s|%13d/%2d|%15s|%30s| \n",UUIDstring.c_str(),nazwisko.c_str(),liczbaWypozyczen,limitPojazdow,nazwaPojazdu.c_str(),ctime (&start));
 
    }

    printf("|______________________________________________________________________________________________________________| \n");


}

void AplicationClass::sortujWypBiezPoNazwisku(){
   
    //  deklaracje zmiennych
    int liczbaWypozyczen,limitPojazdow,idKlienta,idPojazdu;
    boost::uuids::uuid UUID;
    std::string UUIDstring;
    std::string nazwisko;
    std::string nazwaPojazdu;
    time_t start;
    std::shared_ptr<Klient> k;
    std::shared_ptr<Pojazd> p;

    //  deklaracja multimapy z kluczem nazwiska 
    //  multimapa zostala wybrana, poniewaz klucz (czyli nazwisko) moze sie powtarzac
    std::multimap<std::string,std::shared_ptr<Wypozyczenie>> wypozyczenia_b_poNazwisku;

    //  petla kopiujaca (sort po nazwisku)
    for(auto it=wypozyczenia_b.begin();it!=wypozyczenia_b.end();++it){
        
        k=it->second->getKlient();
        nazwisko=k->getNazwisko();
        
        wypozyczenia_b_poNazwisku.insert ( std::pair<std::string,std::shared_ptr<Wypozyczenie>>(nazwisko,it->second) );
        
    }

    //////////////////////  WYSWIETLENIE MAPY POSORTOWANEJ PO Nazwisku/////////////////////////////////////////////////////
    

    printf("|-----------------------------------          Lista Wypozyczen Biezacych      ---------------------------------| \n");
    printf("|                                UUID|    klient|  l. wypozyczen|         pojazd| data rozpoczecia wypozyczenia| \n");
    printf("|--------------------------------------------------------------------------------------------------------------| \n");
    
    for(auto it = wypozyczenia_b_poNazwisku.begin(); it != wypozyczenia_b_poNazwisku.end(); ++it){
        
        //first=it->first;                            //  nr 
        UUID=it->second->getUUID();                 //  UUID
        UUIDstring=boost::uuids::to_string(UUID);   //  UUID (string)
        k=it->second->getKlient();                  //  smart_ptr na klienta
        idKlienta=AplicationClass::getIdKlienta(k); //  id klienta do porownania i wyznaczenia ile posiada aktualnych wypozyczen
        nazwisko=k->getNazwisko();                  //  ustawienie nazwiska jako identyfikator klienta
        
        limitPojazdow=k->getIloscPojazdow();
        
        //  petla sprawdza ile klientow o takim samym id, znajduje sie w mapie wypozyczen biezacych;
        liczbaWypozyczen=0;
        for(auto it = wypozyczenia_b_poNazwisku.begin(); it != wypozyczenia_b_poNazwisku.end(); ++it){
            
            k=it->second->getKlient();                  //  smart_ptr na klienta
            int idK=AplicationClass::getIdKlienta(k);   //  id klienta
            
            if(idKlienta==idK){liczbaWypozyczen++;}
        }
       
        p=it->second->getPojazd();                      //  smart_ptr na pojazd
        nazwaPojazdu=p->getNazwa();                     //  ustawienie nazwy pojazdu jako jego identyfikator
        
         
        start=it->second->getStartDate();
        printf("|%25s|%10s|%13d/%2d|%15s|%30s| \n",UUIDstring.c_str(),nazwisko.c_str(),liczbaWypozyczen,limitPojazdow,nazwaPojazdu.c_str(),ctime (&start));
 
    }

    printf("|______________________________________________________________________________________________________________| \n");


}
/////////////////////   WYPOZYCZENIA   ////////////////////////

void AplicationClass::noweWypozyczenie(){
    
    int nrKlienta,nrPojazdu;
    boost::uuids::uuid  uuid_wyp, nrWypozyczenia;

            
    //  wyswietlenie listy klientow
    AplicationClass::getListaKlientow();
    printf("Podaj numer klienta: \n");
    std::cin>>nrKlienta;
    nrKlienta-=1;
    
    clearscreen();  //czysci ekran
    
//////////  Sprawdzanie czy osiagnieto limit pojazdow   ////////////////////
    Klient *k1;
    std::shared_ptr<Klient> k2;
    
    k1=klienci[nrKlienta];

    int limitPojazdow=k1->getIloscPojazdow();
    
    int liczbaWypozyczen=0;
    
    
        
        for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
            
            k2=it->second->getKlient();                  //  smart_ptr na klienta
            int idK=AplicationClass::getIdKlienta(k2);   //  id klienta
            
            if(nrKlienta==idK){liczbaWypozyczen++;}
        }
    if(liczbaWypozyczen>=limitPojazdow){
        printf("Limit wypozyczonych pojazdow osiagniety! \n");
        
            return;
    }
    

//////////  Wybor pojazdu i sprawdzenie dostepnosci   ////////////////////

        std::shared_ptr<Pojazd> p;
        int idPojazdu;

    
        //  wyswietlenie listy pojazdow
        AplicationClass::getListaDostepnychPojazdow();
    
        printf("Podaj numer pojazdu: \n");
        std::cin>>nrPojazdu;
   
        for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
            
            p=it->second->getPojazd();                      //  smart_ptr na pojazd
            idPojazdu=AplicationClass::getIdPojazdu(p);     //  id pojazdu
            
                if(nrPojazdu==idPojazdu){
                    clearscreen();
                    printf("Pojazd niedostepny! \n");
                        return;
                }
        }
                        
////////////////////    utworzenie wypozyczenia biezacego /////////////////////
    
//  pobranie adresu do obiektu ze wskaznika znajdujacego sie w skopiowanym vectorze
    std::shared_ptr<Klient>  spK(klienci[nrKlienta]);
    std::shared_ptr<Pojazd>  spP(pojazdy[nrPojazdu]);


    std::shared_ptr<Wypozyczenie> spW(new Wypozyczenie(spK,spP));
    uuid_wyp=spW->getUUID();

    
    //  wydruk informacji o utworzonym wypozyczeniu
    std::string info=spW->toString();
    std::cout<<info<<std::endl;
    
    
    //  dodanie wypozyczenia do mapy
    wypozyczenia_b[uuid_wyp]=spW;

    

    printf("Nowe wypozyczenie utworzone ! \n");

    return;
}

void AplicationClass::noweWypozyczenie(int nrPojazdu,int nrKlienta){
    
    
    boost::uuids::uuid  uuid_wyp, nrWypozyczenia;

    
//////////  Sprawdzanie czy osiagnieto limit pojazdow   ////////////////////
    Klient *k1;
    std::shared_ptr<Klient> k2;
    
    k1=klienci[nrKlienta];

    int limitPojazdow=k1->getIloscPojazdow();
    
    int liczbaWypozyczen=0;
    
    
        
        for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
            
            k2=it->second->getKlient();                  //  smart_ptr na klienta
            int idK=AplicationClass::getIdKlienta(k2);   //  id klienta
            
            if(nrKlienta==idK){liczbaWypozyczen++;}
        }
    if(liczbaWypozyczen>=limitPojazdow){
        printf("Limit wypozyczonych pojazdow osiagniety! \n");
        
            return;
    }
    

//////////  Wybor pojazdu i sprawdzenie dostepnosci   ////////////////////

        std::shared_ptr<Pojazd> p;
        int idPojazdu;

        for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
            
            p=it->second->getPojazd();                      //  smart_ptr na pojazd
            idPojazdu=AplicationClass::getIdPojazdu(p);     //  id pojazdu
            
                if(nrPojazdu==idPojazdu){
                    clearscreen();
                    printf("Pojazd niedostepny! \n");
                        return;
                }
        }
                        
////////////////////    utworzenie wypozyczenia biezacego /////////////////////
    
//  pobranie adresu do obiektu 
    std::shared_ptr<Klient>  spK(klienci[nrKlienta]);
    std::shared_ptr<Pojazd>  spP(pojazdy[nrPojazdu]);


    std::shared_ptr<Wypozyczenie> spW(new Wypozyczenie(spK,spP));
    uuid_wyp=spW->getUUID();

    
    //  wydruk informacji o utworzonym wypozyczeniu
    std::string info=spW->toString();
    std::cout<<info<<std::endl;
    spW->losujWypozyczenie();
    
    
    //  dodanie wypozyczenia do mapy
    wypozyczenia_b[uuid_wyp]=spW;

    

    printf("Nowe wypozyczenie utworzone ! \n");

    return;
}

////////////////////    ADDERY  ///////////////////////////////
void AplicationClass::addKlient(){
    std::string imie, nazwisko;
    
    
    for(;;){
        
    clearscreen();  //czysci ekran
        
    std::cout<<"Podaj imie: \n";
    std::cin>>imie;
    std::cout<<"Podaj nazwisko: \n";
    std::cin>>nazwisko;
   
//    std::cout<<" Rozmiar listy przed dodaniem nowego klienta= "<<klienci.size()<<" \n";
      
    klienci.push_back(new Klient(imie,nazwisko));
//    std::cout<<" Rozmiar listy po dodaniu klienta= "<<klienci.size()<<" \n";
    
    std::cout<<"Nowy klient dodany pomyslnie :) \n";
    
    char koniec;
    std::cout<<"Kontynuowac dodawanie? ... (t/N) \n";
    std::cin>>koniec;
    if(koniec=='n' or koniec=='N'){break;};
    
    }
   
}

void AplicationClass::addPojazd() {
    
    int numer;
    float cena,pojemnosc;
    enum seg segment;
    char rodzaj,seg;
    
    for(;;){
    
        clearscreen();  //czysci ekran
        std::cout<<"        !!!Dodawanie pojazdu!!!\n";
        for(;;){
            std::cout<<"Wybierz rodzaj pojazdu \n S-samochod\n M-motocykl\n R-rower\n";
            std::cin>>rodzaj;       
            if(rodzaj=='S' || rodzaj=='s' || rodzaj=='M' || rodzaj=='m' || rodzaj=='R' || rodzaj =='r'){break;}
            else{clearscreen(); std::cout<<" !Bledny rodzaj pojazdu! \n ";}
            }
    
        auto it=pojazdy.end();
        numer=it->first;
        numer+=1;
        
        std::string nazwa;
        
        clearscreen();  //czysci ekran
        std::cout<<"Podaj nazwe pojazdu: \n (bez spacji!) \n";
        //std::getline (std::cin,nazwa);          //  nie wiem czemu nie chce dzialac ?!
        std::cin>>nazwa;
        std::cout<<"Podaj cene pojazdu: \n";
        std::cin>>cena;
        if(rodzaj=='R' or rodzaj=='r'){
        
            pojazdy[numer]=new Rower(nazwa,cena);
           
        }
        
        if(rodzaj=='M' or rodzaj=='m'){
            
            std::cout<<"Podaj pojemnosc :\n";
            std::cin>>pojemnosc;
            pojazdy[numer]=new Motor(nazwa,pojemnosc,cena);
        }

        if(rodzaj=='S' or rodzaj=='s'){
                       
            std::cout<<"Podaj pojemnosc :\n";
            std::cin>>pojemnosc;
            std::cout<<"Podaj segment auta :\n (A|B|C|D|E)\n";
            std::cin>>seg;
            if(seg=='a' or seg =='A'){segment=segA;}
            if(seg=='b' or seg =='B'){segment=segB;}
            if(seg=='c' or seg =='C'){segment=segC;}
            if(seg=='d' or seg =='D'){segment=segD;}
            if(seg=='e' or seg =='E'){segment=segE;}

            pojazdy[numer]=new Samochod(nazwa,pojemnosc,cena,segment);
        }

    
        char koniec;
        std::cout<<"Kontynuowac dodawanie? ... (t/N) \n";
        std::cin>>koniec;
        if(koniec=='n' or koniec=='N'){break;};
    
        }
}

//////////////////  DELETERY    //////////////////////////////



////////////////    LOADER  ///////////////////////////////////

void AplicationClass::loadCustomers() {
    
    std::string imie,nazwisko;
    float obrot,upust;
    char typKlienta;

    std::fstream plikKlienci;
    plikKlienci.open("cust.dat", std::ios::out | std::ios::in);
    
    
    while(!plikKlienci.eof()){

        plikKlienci>>imie;
        plikKlienci>>nazwisko;
        plikKlienci>>obrot;
        plikKlienci>>upust;
        plikKlienci>>typKlienta;
        klienci.push_back(new Klient(imie,nazwisko,obrot,upust,typKlienta));
     
    };
   
    klienci.pop_back();
    
    plikKlienci.close();   
}   

void AplicationClass::loadVehicles() {
        
    std::string nazwa;
    float cena,pojemnosc;
    char rodzajPojazdu,segment;
    seg carSeg;
    int numerPojazdu;           
     

//  Wczytywanie mapy pojazdow z pliku.
//  Przed kazdym pojazdem jest zapisany jego typ, przez co program wie,
//  jakiego typu stworzyc obiekt.
    
    std::fstream plikPojazdy;
    plikPojazdy.open("vehicles.dat", std::ios::out | std::ios::in);
    
    while(!plikPojazdy.eof()){

        //S-samochod, R-rower, M-motocykl
        plikPojazdy>>numerPojazdu;
        plikPojazdy>>rodzajPojazdu;
        plikPojazdy>>nazwa;
        plikPojazdy>>cena;
        plikPojazdy>>pojemnosc;
        plikPojazdy>>segment;
        
        //  wczytywanie SAMOCHODU
        if(rodzajPojazdu=='S') {
            
            //  deklaracja zmiennej pomocniczej enum seg.
            // w przyadku wczytania z pliku segmentu auta (A,B,C,D lub E)
            // zmienia wartosc char na enum seg carSeg (enum segmentu auta)
            seg carSeg;
            if(segment=='A') {
                carSeg=segA;
            }
            
            if(segment=='B') {
                carSeg=segB;
            }
            
            if(segment=='C') {
                carSeg=segC;
            }
            
            if(segment=='D') {
                carSeg=segD;
            }
            
            if(segment=='E') {
                carSeg=segE;
            }

            
            pojazdy[numerPojazdu]=new Samochod(nazwa,pojemnosc,cena,carSeg);
        }
        
         //  wczytywanie ROWERU
        if(rodzajPojazdu=='R') {
            pojazdy[numerPojazdu]=new Rower(nazwa,cena);
            
        }
        
        //  wczytywanie MOTORU
        if(rodzajPojazdu=='M') {
            pojazdy[numerPojazdu]=new Motor(nazwa,pojemnosc,cena);
            
        } 
    };

    plikPojazdy.close();
}


void AplicationClass::loadWypoB() {
     
    int nrKlienta;
    int nrPojazdu;
    boost::uuids::uuid UUID;
    time_t start;
    time_t end;
    float cenaZaDzien;
    
    //  kopiowanie listy klientow na vector
    std::vector<Klient*> vKlienci{ std::begin(klienci), std::end(klienci) };
    
    std::fstream plikWypoB;
    plikWypoB.open("WypoB.dat", std::ios::out | std::ios::in);

    //  sprawdzenie czy plik jest pusty
    int kursor;
    plikWypoB.seekg(0, std::ios::end); // ustawia "kursor" na koniec pliku
    kursor = plikWypoB.tellg(); // znajduje pozycje kursora
    //plikWypoB.close();  //  zamkniecie pliku

    if(kursor == 0 ){
        printf("Plik wypozyczen biezacych jest pusty! \n");
        plikWypoB.close();
        getchar();
        return;         
    }
    
    else{
    plikWypoB.seekg(std::ios::beg); //  ustawienie "kursora" na poczatek pliku
    while(!plikWypoB.eof()){
    
        //  wczytanie informacji z pliku
        plikWypoB>>UUID;
        plikWypoB>>nrKlienta;
        plikWypoB>>nrPojazdu;
        plikWypoB>>UUID;
        plikWypoB>>start;
        plikWypoB>>end;
        plikWypoB>>cenaZaDzien;

        //  zmienna informujaca o tym czy dane UUID juz zostalo zaladowane
        bool dostepnyUUID=true;
        
        //  petla sprawdzajaca mape pod katem duplikatow UUID
        for(auto it=wypozyczenia_b.begin();it!=wypozyczenia_b.end();++it){

            boost::uuids::uuid UUID2;
            UUID2=it->second->getUUID();
                   
            if(UUID==UUID2){
                dostepnyUUID=false;
            }
        }
        
        if(dostepnyUUID==true){
               
            //  pobranie adresu do obiektu ze wskaznika znajdujacego sie w skopiowanym vectorze
            std::shared_ptr<Klient>  spK(vKlienci[nrKlienta]);
            std::shared_ptr<Pojazd>  spP(pojazdy[nrPojazdu]);
        
            //  utworzenie nowego wypozyczenia
            std::shared_ptr<Wypozyczenie> spWB(new Wypozyczenie(spK,spP,UUID,start,end,cenaZaDzien));
            
            //  przypisanie wypozyczenia do mapy.
            wypozyczenia_b[UUID]=spWB;

        }
        
    }
    //  zamkniecie pliku 
    plikWypoB.close();
    printf("Lista wypozyczen bierzacych wczytana!\n");
    getchar();
    return;
    
   }


}

void AplicationClass::loadWypoArch() {
     
    int nrKlienta;
    int nrPojazdu;
    boost::uuids::uuid UUID;
    time_t start;
    time_t end;
    float koszt;
    float roznica;
    
    //  kopiowanie listy klientow na vector
    std::vector<Klient*> vKlienci{ std::begin(klienci), std::end(klienci) };
    
    std::fstream plikWypoArch;
    plikWypoArch.open("WypoArch.dat", std::ios::out | std::ios::in);
    
        
    //  sprawdzenie czy plik jest pusty
    int kursor;
    plikWypoArch.seekg(0, std::ios::end); // ustawia "kursor" na koniec pliku
    kursor = plikWypoArch.tellg(); // znajduje pozycje kursora
 
    
    if(kursor == 0 ){
        printf("Plik wypozyczen archiwalnych jest pusty! \n");
        plikWypoArch.close();
        getchar();
        return;         
    }
    else{
        plikWypoArch.seekg(std::ios::beg);  //  kursor na poczatek
    while(!plikWypoArch.eof()){
    
        //  wczytanie informacji z pliku
        plikWypoArch>>UUID;
        plikWypoArch>>nrKlienta;
        plikWypoArch>>nrPojazdu;
        plikWypoArch>>UUID;
        plikWypoArch>>start;
        plikWypoArch>>end;
        plikWypoArch>>roznica;
        plikWypoArch>>koszt;

        //  zmienna informujaca o tym czy dane UUID juz zostalo zaladowane
        bool dostepnyUUID=true;
        
        //  petla sprawdzajaca mape pod katem duplikatow UUID
        for(auto it=wypozyczenia_arch.begin();it!=wypozyczenia_arch.end();++it){

            boost::uuids::uuid UUID2;
            UUID2=it->second->getUUID();
                   
            if(UUID==UUID2){
                dostepnyUUID=false;
            }
        }
        
        if(dostepnyUUID==true){
               
            //  pobranie adresu do obiektu ze wskaznika znajdujacego sie w skopiowanym vectorze
            std::shared_ptr<Klient>  spK(vKlienci[nrKlienta]);
            std::shared_ptr<Pojazd>  spP(pojazdy[nrPojazdu]);
        
            //  utworzenie nowego wypozyczenia
            std::shared_ptr<Wypozyczenie> spWArch(new Wypozyczenie(spK,spP,UUID,start,end));
            
            //  przypisanie wypozyczenia do mapy.
            wypozyczenia_arch[UUID]=spWArch;
        }
        
    }
    
    //  zamkniecie pliku 
    plikWypoArch.close();
    printf("Wczytano liste wypozyczen archiwalnych! \n");
    getchar();
    }
    
}

    /////////////   SAVER   ///////////////////////////////////
    
void AplicationClass::saver(){
    
    
    
////////////////////////////  zapis klientow    /////////////
    std::string imie,nazwisko;
    float obrot,upust;
    char typKlienta;

    std::fstream plikKlienci;
    plikKlienci.open("cust.dat", std::ios::out | std::ios::in );
    
    for(const auto& Klient : klienci){
        plikKlienci<<Klient->getImie()<<std::endl;
        plikKlienci<<Klient->getNazwisko()<<std::endl;
        plikKlienci<<Klient->getObrot()<<std::endl;
        plikKlienci<<Klient->getUpust()<<std::endl;
        plikKlienci<<Klient->getTypKlienta()<<std::endl;
    }
    
    plikKlienci.close();
    
    
    
////////////////////////////  zapis pojazdow    /////////////    
    std::string pojemnosc,segment;
    std::fstream plikPojazdy;
    plikPojazdy.open("vehicles.dat", std::ios::out | std::ios::in);
    
    for(auto it = pojazdy.cbegin(); it != pojazdy.cend(); ++it){
        
        plikPojazdy<<it->first<<std::endl;
        pojemnosc=it->second->getSTRpojemnosc();
        segment=it->second->getSTRsegment();
        if(pojemnosc=="0"&&segment=="0") {
            plikPojazdy<<'R'<<std::endl;
        }
        
        if(pojemnosc!="0"&&segment=="0") {
            plikPojazdy<<'M'<<std::endl;
        }
        
        if(pojemnosc!="0"&&segment!="0") {
            plikPojazdy<<'S'<<std::endl;
        }

        plikPojazdy<<it->second->getNazwa()<<std::endl;
        plikPojazdy<<it->second->getSTRcenaBazowa()<<std::endl;
        plikPojazdy<<it->second->getSTRpojemnosc()<<std::endl;
        plikPojazdy<<it->second->getSTRsegment()<<std::endl;
        
        }
    plikPojazdy.close();
    
    
////////////////////////////  zapis wypozyczen biezacych    /////////////
    std::shared_ptr<Klient> k;
    std::shared_ptr<Pojazd> p;
    int idKlienta,idPojazdu;
    float cena;
    float cenaZaDzien;
    
    std::fstream plikWypoB;
    plikWypoB.open("WypoB.dat", std::ios::out | std::ios::in);
    
    for(auto it = wypozyczenia_b.begin(); it != wypozyczenia_b.end(); ++it){
        
        k=it->second->getKlient();
        idKlienta=AplicationClass::getIdKlienta(k);
        upust=k->getUpust();
        
        p=it->second->getPojazd();
        idPojazdu=AplicationClass::getIdPojazdu(p);
        cena=p->getCenaZaDzien();
        
        cenaZaDzien=cena*upust;
        
        plikWypoB<<it->second->getUUID()<<std::endl;        //nr wypozyczenia czyli UUID
        plikWypoB<<idKlienta<<std::endl;                    //nr klienta
        plikWypoB<<idPojazdu<<std::endl;                    //nr pojazdu
        plikWypoB<<it->second->getUUID()<<std::endl;        //UUID
        plikWypoB<<it->second->getStartDate()<<std::endl;   //data rozpoczecia
        plikWypoB<<it->second->getEndDate()<<std::endl;     //data zakonczenia
        plikWypoB<<cenaZaDzien<<std::endl;                            //cenaZaDzien
        
        }
    plikWypoB.close();
    
////////////////////////////  zapis wypozyczen  arch   /////////////    
    
    std::fstream plikWypoArch;
    plikWypoArch.open("WypoArch.dat", std::ios::out | std::ios::in);
    
    for(auto it = wypozyczenia_arch.begin(); it != wypozyczenia_arch.end(); ++it){
        
        k=it->second->getKlient();
        idKlienta=AplicationClass::getIdKlienta(k);
        p=it->second->getPojazd();
        float liczbaDni=it->second->getDiffTime();
        idPojazdu=AplicationClass::getIdPojazdu(p);
        float cena=it->second->getCena();
        float koszt=cena*liczbaDni;                            //  calokowity koszt wypozyczenia
        float zaok_2 = ceilf(koszt * 100) / 100;               //  zaokraglanie do 2 miejsc po przecinku
        koszt=zaok_2;                                          //  przy uzyciu math.h
        
        plikWypoArch<<it->second->getUUID()<<std::endl;        //nr wypozyczenia czyli UUID
        plikWypoArch<<idKlienta<<std::endl;                    //nr klienta
        plikWypoArch<<idPojazdu<<std::endl;                    //nr pojazdu
        plikWypoArch<<it->second->getUUID()<<std::endl;        //UUID
        plikWypoArch<<it->second->getStartDate()<<std::endl;   //data rozpoczecia
        plikWypoArch<<it->second->getEndDate()<<std::endl;     //data zakonczenia
        plikWypoArch<<liczbaDni<<std::endl;                    //ilosc dni
        plikWypoArch<<koszt<<std::endl;                        //koszt
        
        
        }
    plikWypoB.close();

} 
//  funkcja uzywa odpowiedniej f-cji czyszczacej, zaleznie od 
//  systemu na jakim jest uruchomiony program
void AplicationClass::clearscreen(){
    #ifdef WIN32
    system("cls");
    #else
    system("clear");
    #endif
}


void AplicationClass::startSimulation(){
           
    bool pojazdDostepny;
    bool klientLimit;
    bool zakWyp;
    
    srand( time( NULL ) );
    
    char zakonczenieWypozyczenia;
    printf("Wlaczyc symulacje zakonczenia losowego wypozyczenia w trakcie przebiegow? (t/n)");
    std::cin>>zakonczenieWypozyczenia;
    if(zakonczenieWypozyczenia=='t'||zakonczenieWypozyczenia=='T'){
        zakWyp=true;        
    }
    else{zakWyp=false;};
    getchar();
    
    for(int i=1;i<11;i++){
        
        clearscreen();
        
        pojazdDostepny=true;
        
        printf("przebieg nr %2d \n",i);

        
        if(zakWyp==true){
            //  sprawdzenie dystansu pomiedzy pocztkiem i koncem mapy    
            int dystans=std::distance(wypozyczenia_b.begin(),wypozyczenia_b.end());

            // jezeli jest rozna od zera, oznacza to ze nie jest pusta i mozna zakonczyc wypozyczenie
            if(dystans!=false){
                AplicationClass::zakonczWypozyczenie(true);
            }   
        }
       
        //  losowanie klienta i pojazdu do utworzenia wypozyczenia


        int nrKlienta =(std::rand() % 4 ) + 1;
        int nrPojazdu = std::rand() % 9 ;

        std::cout<<nrKlienta<<" "<<klienci[nrKlienta]->getName()<<"\n";
        std::cout<<nrPojazdu<<" "<<pojazdy[nrPojazdu]->getNazwa()<<"\n";
        
        
        AplicationClass::noweWypozyczenie(nrPojazdu,nrKlienta);
        AplicationClass::getListaWypozyczenBiezacych();
        AplicationClass::getListaWypozyczenArchiwalnych();

        getchar();
    }
    
    clearscreen();
    printf("Koniec przebiegow. \n");
    
        //  ostrzezenie i mozliwosc przerwania symulacji
    char uwaga;
    printf("UWAGA! CZY ZAKONCZYC WSZYSTKIE WYPOZYCZENIA BIERZACE? (T/N)\n");
    std::cin>>uwaga;
    if(uwaga=='T' || uwaga=='t'){
    
    
    //  konczenie pozostalych wypozyczen
    boost::uuids::uuid UUID;
    std::shared_ptr<Klient> k;
    for(auto it=wypozyczenia_b.begin(); it!=wypozyczenia_b.end();++it){
       
            UUID=it->second->getUUID();
            it->second->setEnd();
            float liczbaDni=it->second->getDiffTime();
            float cena=it->second->getCena();
            
            //  pobranie klienta i jego aktualnego obrotu
            k=it->second->getKlient();
            float obrot=k->getObrot();
           
            //  ustawienie nowego obrotu oraz wyliczenie nowego upustu w oparciu
            //  o aktualny obrot. Calkowity koszt wypozyczenia zostaje zaokraglony
            //  do 2 miejsc po przecinku.
            float koszt=(cena*liczbaDni);
            std::cout<<"Calkowity koszt wypozyczenia wynosi: "<<koszt<<std::endl;
            float zaok_2 = ceilf(koszt * 100) / 100;
            koszt=zaok_2;
            std::cout<<"Po zaokragleniu: "<<koszt<<std::endl;
            obrot+=koszt;
            k->setObrot(obrot);
            k->setWyliczonyNowyUpust();
            
            //  skopiowanie wypozyczenia do wypozyczen archiwalnych
            wypozyczenia_arch[UUID]=it->second;
            
            printf("Wypozyczenie zakonczone pomyslnie. \n");            

        }
        wypozyczenia_b.clear();
    
        AplicationClass::getListaKlientow();
        AplicationClass::getListaWypozyczenBiezacych();
        AplicationClass::getListaWypozyczenArchiwalnych(); 
        
    }
    
    printf("KONIEC SYMULACJI!!!\n");
    getchar();

    
}