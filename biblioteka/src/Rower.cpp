/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Rower.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 1 grudnia 2016, 17:34
 */



#include "../include/Rower.h"



Rower::Rower() {
}

/*
 * Konstruktor parametrowy ustawia cene za dzien wypozyczenia roweru
 * oraz ustawia pojemnosc roweru na 1.0
 */
Rower::Rower(std::string nazwa, float cenaPojazdu)
:Pojazd(nazwa,cenaPojazdu) {
    
    Pojazd::setCenaZaDzien(cenaPojazdu);


}

Rower::~Rower() {
}

///////////////////////////SETTERY//////////////////////////////////////////////

///////////////////////      GETTERY             ///////////////////////////////
std::string Rower::getSTRcenaBazowa(){
 
    //  konwersja ceny pojazdu (float) na string
    std::string cenaBazowaRoweru = std::to_string(Rower::getCenaPojazdu());
    
    //  usuniecie z konca stringa 4 cyfr w celu unikniecia wyswietlania zbyt
    //  dlugiej liczby. Np.: 50.000000
    cenaBazowaRoweru.erase( cenaBazowaRoweru.size()-4, 4 );

        return cenaBazowaRoweru;    
}

std::string Rower::getSTRcena() {
 
    //  konwersja ceny pojazdu (float) na string
    std::string cenaRoweru = std::to_string(Rower::getCenaZaDzien());
    
    //  usuniecie z konca stringa 4 cyfr w celu unikniecia wyswietlania zbyt
    //  dlugiej liczby. Np.: 50.000000
    cenaRoweru.erase( cenaRoweru.size()-4, 4 );

        return cenaRoweru;
}

std::string Rower::getSTRpojemnosc() {

    return "0";
}

std::string Rower::getSTRsegment() {

    return "0";
}
