/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Pojazd.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 1 grudnia 2016, 17:21
 */

#include <math.h>
#include <cstdio>
#include <string>
#include <stdexcept>

#include "../include/Pojazd.h"

Pojazd::Pojazd() {
}

/*
 * Konstruktor parametrowy ustawia cene za dzien wypozyczenia pojazdu
 */
Pojazd::Pojazd(std::string nazwa, float cenaPojazdu)
:nazwa(nazwa),cenaPojazdu(cenaPojazdu)
{
    Pojazd::setCenaZaDzien(cenaPojazdu);
}



Pojazd::~Pojazd() {
}

///////////////////////////////////////GETTERY//////////////////////////////////


    std::string Pojazd::getNazwa() {
    return nazwa;
    }
    
   /*
    * Pobranie ceny pojazdu
    */
    float Pojazd::getCenaPojazdu() {
        return cenaPojazdu;
    }
    
   /*
    * Pobranie ceny pojazdu za dzien
    */
    float Pojazd::getCenaZaDzien() {
        return cenaZaDzien;
    }
    
 
//   /*
//    * Pobranie dostepnosci pojazdu
//    * typ logiczny bool zwraca prawde(true) lub falsz(false)
//    * jezeli auto nie jest wypozyczone, czyli jest dostepne to 'bool dostepny'
//    * powinien byc ustawiony na true, a jezeli jest wypozyczony to na false.
//    */
//    }
//    bool Pojazd::getDostepny() {
//        return dostepny;
//    }
    
    

////////////////////////////////////SETTERY/////////////////////////////////////

    
   /*
    * Ustawienie ceny pojazdu
    */
    void Pojazd::setCenaPojazdu(float nowaCena) {
        try{
            if(nowaCena<0){
                throw std::logic_error("ERROR #152 - ujemna cena pojazdu!\n");
            }
        }
        
        catch(std::logic_error &error){
            printf(error.what());
            getchar();
            return;
        }
        
        
        cenaPojazdu=nowaCena;
    }
    

    
   /*
    * Ustawienie ceny za dzien wypozyczenia 'na sztywno'
    */
    void Pojazd::setCenaZaDzien(float sztywnaCenaZaDzien) {
        
        try{
            if(sztywnaCenaZaDzien<0){
                throw std::logic_error("ERROR #152 - ujemna cena pojazdu!\n");
            }
        }
        
        catch(std::logic_error &error){
            printf(error.what());
            getchar();
            return;
        }
        
        cenaZaDzien=sztywnaCenaZaDzien;
    }
