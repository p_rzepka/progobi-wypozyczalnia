/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TypKlienta.cpp
 * Author: Przemysław Rzepka i Radosław Kępa
 * 
 * Created on 2 grudnia 2016, 16:52
 */

#include "../include/TypKlienta.h"


 ////////////////////   A - STANDARD - CONST   ///////////////////////////
//___________________________________________________//
TypStandard::TypStandard(int ile) {
    this->liczbaPojazdow=ile;
}

TypStandard::~TypStandard() {
    
}

//%%% GETTERY
int TypStandard::getLiczbaPojazdow(){
    return liczbaPojazdow;
    }

float TypStandard::getUpust(float obrot,float upust) {
   return 1.0;
}

std::string TypStandard::getOpisTypKlienta() {
    std::string nazwa = "-typ: 'Standard'";// - upust staly";
    return nazwa;
}

char TypStandard::getTypKlienta() {
    return 'A';
}








 ////////////////////   B - Loyal - SCHODKOWA   ///////////////////////////
//___________________________________________________//
TypLoyal::TypLoyal(int ile) {
    this->liczbaPojazdow=ile;
}

TypLoyal::~TypLoyal() {
    
}


//%%% GETTERY

/*
 * Zwraca liczbe pojazdow
 */
int TypLoyal::getLiczbaPojazdow(){
    return liczbaPojazdow;
    }
/*
 * zwraca nowy upust naliczony schodkowo ,
 * (po przekroczeniu danego progu wzrasta upust),
 * z uwzglednieniem dotychczasowego upustu i obrotu
 */
float TypLoyal::getUpust(float obrot,float upust) {
    float rabat;
    if (obrot<1000) {
        rabat=0.0;
    }
    
    if (obrot>=1000 && obrot<3000) {
        rabat=0.05;
    }
    
    if (obrot>=3000 && obrot<4000) {
        rabat=0.1;
    }
    
    if (obrot>=4000 && obrot<6000) {
        rabat=0.15;
    }
    
    if (obrot>=6000 && obrot<8000) {
        rabat=0.2;
    }
    
    if (obrot>10000) {
        rabat=0.25;
    }
    
    return upust-=rabat;
}

/*
 * Zwraca string z opisem typu klienta (nazwa + typ naliczania upustu
 */
std::string TypLoyal::getOpisTypKlienta() {
    std::string nazwa = "-typ: 'Loyal'";// - upust naliczany schodkowo";
    return nazwa;
}

char TypLoyal::getTypKlienta() {
    return 'B';
}





 ////////////////////   C  - Buisness  ///////////////////////////
//___________________________________________________//
TypBuisness::TypBuisness(int ile) {
    this->liczbaPojazdow=ile;
}

TypBuisness::~TypBuisness() {
    
}
//%%% GETTERY
int TypBuisness::getLiczbaPojazdow(){
    return liczbaPojazdow;
    }

/*
 * zwraca nowy upust naliczony liniowo ,
 * (im wiekszy obrot tym wiekszy upust),
 * z uwzglednieniem dotychczasowego upustu i obrotu
 * korzysta z wzoru obrot/100 000
 */
float TypBuisness::getUpust(float obrot,float upust) {
    float rabat;
    rabat=(obrot/100000);
    return upust-=rabat;
}



std::string TypBuisness::getOpisTypKlienta() {
    std::string nazwa = "-typ: 'Buisness'"; //- upust naliczany liniowo";
    return nazwa;
}

char TypBuisness::getTypKlienta() {
    return 'C';
}

        
 ////////////////////   D  - Buisness PRO  ///////////////////////////
//___________________________________________________//
TypBuisnessPRO::TypBuisnessPRO(int ile) {
    this->liczbaPojazdow=ile;
}

TypBuisnessPRO::~TypBuisnessPRO() {
    
}
//%%% GETTERY
int TypBuisnessPRO::getLiczbaPojazdow(){
    return liczbaPojazdow;
    }
/*
 * zwraca nowy upust naliczony liniowo-szybko ,
 * (im wiekszy obrot tym wiekszy upust),
 * z uwzglednieniem dotychczasowego upustu i obrotu
 * korzysta z wzoru obrot/75 000
 */
float TypBuisnessPRO::getUpust(float obrot,float upust) {
    float rabat;
    rabat=(obrot/75000);
    return upust-=rabat;
}

std::string TypBuisnessPRO::getOpisTypKlienta() {
    std::string nazwa = "-typ: 'BuisnessPRO'";// - upust naliczany liniowo (szybko)";
    return nazwa;
}

char TypBuisnessPRO::getTypKlienta() {
    return 'D';
}
